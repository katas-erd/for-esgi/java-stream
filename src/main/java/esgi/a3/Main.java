package esgi.a3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;


public class Main {
  public static void main(String[] args) {

    List<Student> people = Arrays.asList(
        new Student("Wendy", 2020, Arrays.asList(7, 0, 2, 12, 10), 1),
        new Student("Mike", 2020, Arrays.asList(12, 13, 18, 17, 18), 2),
        new Student("Faythe", 2020, Arrays.asList(4, 16, 6, 17, 12), 3),
        new Student("Chuck", 2020, Arrays.asList(19, 5, 0, 16, 18), 1),
        new Student("Ted", 2020, Arrays.asList(8, 11, 0, 16, 18), 3),
        new Student("Bob", 2020, Arrays.asList(4, 10, 8, 12, 16), 2),
        new Student("Mallory", 2019, Arrays.asList(8, 13, 0, 11, 18), 4),
        new Student("Alice", 2020, Arrays.asList(1, 6, 19, 10, 17), 1),
        new Student("Rupert", 2018, Arrays.asList(7, 12, 16, 13, 15), 2),
        new Student("Heidi", 2019, Arrays.asList(18, 2, 8, 10, 14), 3),
        new Student("Peggy", 2020, Arrays.asList(14, 16, 10, 15, 9), 4),
        new Student("Grace", 2020, Arrays.asList(16, 5, 12, 9, 15), 4),
        new Student("Ivan", 2018, Arrays.asList(10, 2, 9, 6, 3), 3),
        new Student("Eve", 2020, Arrays.asList(13, 12, 15, 19, 14), 2),
        new Student("Trudy", 2020, Arrays.asList(12, 5, 16, 9, 4), 1),
        new Student("Walter", 2020, Arrays.asList(9, 14, 4, 12, 7), 1),
        new Student("Sybil", 2015, Arrays.asList(3, 19, 17, 15, 2), 4),
        new Student("Dan", 2018, Arrays.asList(12, 9, 5, 4, 3), 2),
        new Student("Oscar", 2019, Arrays.asList(3, 0, 5, 9, 15), 3),
        new Student("Judy", 2020, Arrays.asList(19, 18, 12, 1, 5), 1),
        new Student("Craig", 2019, Arrays.asList(13, 10, 6, 12, 16), 3),
        new Student("Carol", 2017, Arrays.asList(8, 7, 11, 4, 14), 2),
        new Student("Erin", 2018, Arrays.asList(0, 7, 15, 1, 8), 4),
        new Student("Olivia", 2020, Arrays.asList(3, 13, 7, 2, 10), 1),
        new Student("Frank", 2020, Arrays.asList(11, 19, 5, 4, 8), 2),
        new Student("Victor", 2018, Arrays.asList(20, 17, 19, 6, 9), 3)
    );

    System.out.println("Get the name of all students");
    // Wendy, Mike, Faythe, Chuck, Ted, Bob, Mallory, Alice, Rupert, Heidi, Peggy, Grace, Ivan, Eve, Trudy, Walter, Sybil, Dan, Oscar, Judy, Craig, Carol, Erin, Olivia, Frank, Victor

    System.out.println("\nGet all students who has joined the group in 2018");
    // Rupert, Ivan, Dan, Erin, Victor
    System.out.println("\nGet the number of students who has joined the group AFTER 2018");
    System.out.println("\nGet the oldest prom year");
    System.out.println("\nGet the number of groups");

    System.out.println("\nGet the size of the group 2");

    System.out.println("\nGet the names of students in the group 3");

    System.out.println("\nGet the names of the first student who has joined the group 4");

     System.out.println("\nTODO Get the prom year of Mike, Carol and Jane, if they exist");


    System.out.println("\nFind the names of the students who have an average between 9 and 10");


    System.out.println("\nGet the name of all student sorted in alphabetical order (use map and sorted)");
    // TODO Get the global average (use mapFlat)
    System.out.println("\nGet the global average (use mapFlat)");
    // moyenne générale
    // ~10.15

    System.out.println("\nGet the name of all student with an average below 10");
    // Carol, Dan, Erin, Frank, Ivan, Olivia, Oscar, Trudy, Walter, Wendy

    System.out.println("\nGet the name of all student who has repeated at least one year");
    // Mallory, Rupert, Heidi, Ivan, Sybil, Dan, Oscar, Craig, Carol, Erin, Victor

    System.out.println("\nGet the name and the rank (based on their average) of each people. Sorted by rank");
    // 1 [Ivan], 2 [Wendy, Erin], 3 [Oscar], 4 [Dan], 5 [Olivia], 6 [Carol], 7 [Trudy, Walter], 8 [Frank], 9 [Bob, Mallory], 10 [Heidi], 11 [Ted, Alice], 12 [Faythe, Judy], 13 [Sybil], 14 [Grace, Craig], 15 [Chuck], 16 [Rupert], 17 [Peggy], 18 [Victor], 19 [Eve], 20 [Mike]

    System.out.println("\nGet the name and the average of each people in the group sorted by alphabetical order");
    // Alice 10.6, Bob 10.0, Carol 8.8, Chuck 11.6, Craig 11.4, Dan 6.6, Erin 6.2, Eve 14.6, Faythe 11.0, Frank 9.4, Grace 11.4, Heidi 10.4, Ivan 6.0, Judy 11.0, Mallory 10.0, Mike 15.6, Olivia 7.0, Oscar 6.4, Peggy 12.8, Rupert 12.6, Sybil 11.2, Ted 10.6, Trudy 9.2, Victor 14.2, Walter 9.2, Wendy 6.2

    // TODO Get the names of each student in each subgroup sorted by alphabetical order
    // 1 [Wendy, Chuck, Alice, Trudy, Walter, Judy, Olivia], 2 [Mike, Bob, Rupert, Eve, Dan, Carol, Frank], 3 [Faythe, Ted, Heidi, Ivan, Oscar, Craig, Victor], 4 [Mallory, Peggy, Grace, Sybil, Erin]

    // TODO Get the average of each subgroup
    // Group #1 9.257142857142858, Group #2 11.085714285714285, Group #3 10.0, Group #4 10.32

    // TODO Get the hardest exam (i.e. the one with the most scores below 10).
    // Hardest exam is #3


    // TODO Do people who has repeated at least one year have better scores than others ? Display True or False
    
  }




}
