package esgi.a3;

import java.util.List;

public class Student {
    Student(String name, Integer promYear, List<Integer> scores, Integer groupNumber) {
      this.name = name;
      this.scores = scores;
      this.promYear = promYear;
      this.groupNumber = groupNumber;
    }

    public String getName() {
      return name;
    }

    public List<Integer> getScores() {
      return scores;
    }

    public Integer getPromYear() {
      return promYear;
    }

    public Integer getGroupNumber() {
      return groupNumber;
    }

    private String name;
    private List<Integer> scores;
    private Integer promYear;
    private Integer groupNumber;
}
